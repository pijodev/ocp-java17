package org.ocp.examples;

import java.io.Serializable;

// ASTUCE :
// Tant qu'il n'y a pas top-level classe PUBLIQUE dans la classe, on peut appeler le fichier comme on veut.

// Ce qui est sérializable tout SAUF :
// champs static
// champs transient
// objets pas serializables

import java.io.*;

class Bird {
    protected transient String transientColor;
    protected static String staticVar; // trust me this is not serializable
    protected String name;

    public String getName() { return name; }
    public Bird() {
        System.out.println("constructeur de Bird - PAS SERIALIZABLE");
        this.name = "Matt-The-Bird";
        this.transientColor = "color-bird";
        this.staticVar = "staticBird - trust me this is not serializable";
    }

    @Override
    public String toString() {
        return "Bird{" +
                "colorTransient='" + transientColor + '\'' +
                ", name='" + name + '\'' +
                ", staticVariable='" + staticVar + '\'' +
                '}';
    }
}

class Eagle extends Bird implements Serializable {
    protected transient String transientColor;
    protected static String staticVar; // trust me this is not serializable
    protected String name;

    public Eagle() {
        System.out.println("constructeur de Eagle"); // super() avant cette ligne
        this.name = "Bridget-The-Eagle";
        this.transientColor = "gray-eagle";
        this.staticVar = "staticEagle - trust me this is not serializable";
    }
    @Override
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Eagle{" +
                "colorTransient='" + transientColor + '\'' +
                ", name='" + name + '\'' +
                ", staticVariable='" + staticVar + '\'' +
                '}';
    }
}

class Main {
    public static void main(String[] args) {
        Eagle e = new Eagle();
        System.out.println("Instance = " + e.name);
        e.name = "Adeline";
        System.out.println("Instance = " + e.getName());
        System.out.println("eagle to be serialized = " + e.toString());
        String filePath = "serializationExample.txt";

        try (FileOutputStream fileOutputStream = new FileOutputStream(filePath);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream)) {
            System.out.println("Serialization - WRITE Eagle");
            objectOutputStream.writeObject(e);
            Eagle.staticVar = "the static var changed in the context so deserialization will show this";
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }

        try (
                FileInputStream fileInputStream = new FileInputStream(filePath);
                ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        ) {
            System.out.println("Deserialization - READ Eagle");
            Eagle deserializedEagle = (Eagle) objectInputStream.readObject();
            System.out.println(deserializedEagle.toString());
        } catch (IOException | ClassNotFoundException ex) {
            throw new RuntimeException(ex);
        }
    }
}